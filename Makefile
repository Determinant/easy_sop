.PHONY: all
%.pdf: %.tex sop_header.tex
	cd $(dir $@); xelatex -output-driver="xdvipdfmx -V3" -shell-escape $(notdir $<)
PROG := Columbia Cornell
all: $(addsuffix /sop.pdf,$(PROG))
